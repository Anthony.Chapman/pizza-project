import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PizzasListComponent } from './components/pizzas-list/pizzas-list.component';
import { CreatePizzaPageComponent } from './components/create-pizza-page/create-pizza-page.component';
import { HomeComponent } from './components/home/home.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { ContactPageComponent } from './components/contact-page/contact-page.component';
import { CartComponent } from './components/cart/cart.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'createpizza', component: CreatePizzaPageComponent },
  { path: 'pizzas', component: PizzasListComponent },
  { path: 'about-us', component: AboutUsComponent },
  { path: 'contact', component: ContactPageComponent },
  { path: 'cart', component: CartComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
