import { Component, OnInit } from '@angular/core';
import { Pizza } from 'src/app/shared/pizza';
import { SunService } from 'src/app/sun.service';
import { Topping } from '../create-pizza-page/Topping';
import { PIZZAS } from "src/app/shared/mock-pizzas";
import { query } from '@angular/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { CartService } from 'src/app/cart.service';


@Component({
  selector: 'app-pizzas-list',
  templateUrl: './pizzas-list.component.html',
  styleUrls: ['./pizzas-list.component.css']
})
export class PizzasListComponent implements OnInit {
  pizzas: Pizza[];
  pizzaPrice = PIZZAS;
  
  selectedItem: Pizza;

  topping:Topping;
  selected:boolean = false;
  small:number = 0;
  medium:number = 1;
  large:number = 2;

  basePrice:number = 0;
  totalPrice:number = 0;

  size:number = 1;

  message:boolean = false;


  constructor(
    private sunService: SunService,
    private route: ActivatedRoute,
    private cartService: CartService,
    private router: Router
    ) { }

  getPizzas(): void {
    this.sunService.getPizzas()
    .subscribe(pizzas => this.pizzas = pizzas);
  }
  selectItem(item): void {
    this.message = false;
    this.selectedItem = item;
    this.setPrice();
  }

  ngOnInit() {
    this.getPizzas();
  }

  quantity:number = 1;
  i = 1;
  minus() {
    if(this.quantity > 1){
      this.quantity--;
      this.totalPrice = this.totalPrice - (this.selectedItem.price + this.size);
    }
  }
  plus() {
    if(this.quantity < 10){
      this.quantity++;
      console.log((this.selectedItem.price + this.size) * this.quantity);
      
      this.totalPrice = (this.selectedItem.price + this.size) * this.quantity;
    }
  }

  getItemPrice() {
    
  }

  setPrice() {
    this.totalPrice = (this.selectedItem.price + this.size) * this.quantity;
  }

  onToggleSmall() {
    this.size = 0;
    this.totalPrice = this.selectedItem.price * this.quantity;
  }

  onToggleMedium() {
    this.size = 1;
    this.totalPrice = (this.selectedItem.price + 1) * this.quantity;
  }

  onToggleLarge() {
    this.size = 2;
    this.totalPrice = (this.selectedItem.price + 2) * this.quantity;
  }

  

  addToCart(selectedItem) {
    const item:any = selectedItem;
    item.quantity = this.quantity;
    item.price = this.totalPrice;
    this.cartService.addToCart(item);
    this.messageFunc();
  }

  messageFunc() {
    this.message = true;
  }

  goToCart = function() {
  this.router.navigateByUrl('/cart');
  }

}

