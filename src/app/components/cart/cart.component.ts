import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/cart.service';
import { Cart } from 'src/app/shared/cart';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  carts : Cart[];

  getCart(): void {
    this.cartService.getCart()
    .subscribe(cart => this.carts = cart);
  }

  constructor(
    private cartService: CartService
  ) { }

  ngOnInit(): void {
    this.getCart();
  }

}
