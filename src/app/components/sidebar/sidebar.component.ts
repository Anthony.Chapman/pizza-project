import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
})

export class SidebarComponent implements OnInit {
  _opened:boolean = false;
  _modeNum: number = 0;
  _positionNum: number = 0;

  _MODES: Array<string> = ['over', 'push', 'slide'];
  _POSITIONS: Array<string> = ['left', 'right', 'top', 'bottom'];
  
  ngOnInit(): void {
  }

  _toggleOpened(): void {
    this._opened = !this._opened;
  }
}
