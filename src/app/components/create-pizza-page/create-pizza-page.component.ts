import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create-pizza-page',
  templateUrl: './create-pizza-page.component.html',
  styleUrls: ['./create-pizza-page.component.css'],
})
export class CreatePizzaPageComponent implements OnInit {
  crust:boolean = true;
  
  size:string = "Medium";
  small:boolean = false;
  medium:boolean = true;
  large:boolean = false;
  
  sauce:boolean = true;
  sauceChoice:string = "Yes";

  cheese:boolean = true;
  cheeseChoice:string = "Yes";

  feta:boolean = false;
  blueCheese:boolean = false;
  mozzarella:boolean = false;

  pepperoni:boolean = false;
  ham:boolean = false;
  bacon:boolean = false;
  chicken:boolean = false;
  groundBeef:boolean = false;
  pork:boolean = false;
  shrimp:boolean = false;

  tomato:boolean = false;
  pineapple:boolean = false;
  mushroom:boolean = false;
  onion:boolean = false;
  pepper:boolean = false;
  garlic:boolean = false;
  olive:boolean = false;

  basePrice:number = 0;
  toppingsPrice:number = 0;
  totalPrice:number = 0;

  constructor() { }

  ngOnInit(): void { 
    this.setPrice();
  }

  setPrice() {
    this.basePrice = 6;
    this.totalPrice = this.basePrice + this.toppingsPrice;
  }

  onToggleSmall() {
    this.small = !this.small;
    this.size = "Small";
    this.basePrice = 5;
    this.totalPrice = this.basePrice + this.toppingsPrice;
  }

  onToggleMedium() {
    this.medium = !this.medium;
    this.size = "Medium";
    this.basePrice = 6;
    this.totalPrice = this.basePrice + this.toppingsPrice;
  }

  onToggleLarge() {
    this.large = !this.large;
    this.size = "Large";
    this.basePrice = 7;
    this.totalPrice = this.basePrice + this.toppingsPrice;
  }

  onToggleSauce() {
    this.sauce = !this.sauce;
    if (this.sauce) {
      this.sauceChoice = "Yes";
      this.toppingsPrice += 1;
    } else {
      this.sauceChoice = "No";
      this.toppingsPrice -= 1;
    }
    this.totalPrice = this.basePrice + this.toppingsPrice;
  }

  onToggleCheese() {
    this.cheese = !this.cheese;
    if (this.cheese) {
      this.cheeseChoice = "Yes";
      this.toppingsPrice += 1;
    } else {
      this.cheeseChoice = "No";
      this.toppingsPrice -= 1;
    }
    this.totalPrice = this.basePrice + this.toppingsPrice;
  }

  onToggleFeta() {
    this.feta = !this.feta;
    if (this.feta) {
      this.toppingsPrice += .50;
    } else {
      this.toppingsPrice -= .50;
    }
    this.totalPrice = this.basePrice + this.toppingsPrice;
  }

  onToggleBlueCheese() {
    this.blueCheese = !this.blueCheese;
    if (this.blueCheese) {
      this.toppingsPrice += .50;
    } else {
      this.toppingsPrice -= .50;
    }
    this.totalPrice = this.basePrice + this.toppingsPrice;
  }

  onToggleMozzarella() {
    this.mozzarella = !this.mozzarella;
    if (this.mozzarella) {
      this.toppingsPrice += .50;
    } else {
      this.toppingsPrice -= .50;
    }
    this.totalPrice = this.basePrice + this.toppingsPrice;
  }

  onTogglePepperoni() {
    this.pepperoni = !this.pepperoni;
    if (this.pepperoni) {
      this.toppingsPrice += .50;
    } else {
      this.toppingsPrice -= .50;
    }
    this.totalPrice = this.basePrice + this.toppingsPrice;
  }

  onToggleHam() {
    this.ham = !this.ham;
    if (this.ham) {
      this.toppingsPrice += .50;
    } else {
      this.toppingsPrice -= .50;
    }
    this.totalPrice = this.basePrice + this.toppingsPrice;
  }

  onToggleBacon() {
    this.bacon = !this.bacon;
    if (this.bacon) {
      this.toppingsPrice += .50;
    } else {
      this.toppingsPrice -= .50;
    }
    this.totalPrice = this.basePrice + this.toppingsPrice;
  }

  onToggleChicken() {
    this.chicken = !this.chicken;
    if (this.chicken) {
      this.toppingsPrice += .50;
    } else {
      this.toppingsPrice -= .50;
    }
    this.totalPrice = this.basePrice + this.toppingsPrice;
  }

  onToggleGroundBeef() {
    this.groundBeef = !this.groundBeef;
    if (this.groundBeef) {
      this.toppingsPrice += .50;
    } else {
      this.toppingsPrice -= .50;
    }
    this.totalPrice = this.basePrice + this.toppingsPrice;
  }

  onTogglePork() {
    this.pork = !this.pork;
    if (this.pork) {
      this.toppingsPrice += .50;
    } else {
      this.toppingsPrice -= .50;
    }
    this.totalPrice = this.basePrice + this.toppingsPrice;
  }

  onToggleShrimp() {
    this.shrimp = !this.shrimp;
    if (this.shrimp) {
      this.toppingsPrice += .50;
    } else {
      this.toppingsPrice -= .50;
    }
    this.totalPrice = this.basePrice + this.toppingsPrice;
  }

  onToggleTomato() {
    this.tomato = !this.tomato;
    if (this.tomato) {
      this.toppingsPrice += .50;
    } else {
      this.toppingsPrice -= .50;
    }
    this.totalPrice = this.basePrice + this.toppingsPrice;
  }

  onTogglePineapple() {
    this.pineapple = !this.pineapple;
    if (this.pineapple) {
      this.toppingsPrice += .50;
    } else {
      this.toppingsPrice -= .50;
    }
    this.totalPrice = this.basePrice + this.toppingsPrice;
  }

  onToggleMushroom() {
    this.mushroom = !this.mushroom;
    if (this.mushroom) {
      this.toppingsPrice += .50;
    } else {
      this.toppingsPrice -= .50;
    }
    this.totalPrice = this.basePrice + this.toppingsPrice;
  }

  onToggleOnion() {
    this.onion = !this.onion;
    if (this.onion) {
      this.toppingsPrice += .50;
    } else {
      this.toppingsPrice -= .50;
    }
    this.totalPrice = this.basePrice + this.toppingsPrice;
  }

  onToggleRedPepper() {
    this.pepper = !this.pepper;
    if (this.pepper) {
      this.toppingsPrice += .50;
    } else {
      this.toppingsPrice -= .50;
    }
    this.totalPrice = this.basePrice + this.toppingsPrice;
  }

  onToggleGarlic() {
    this.garlic = !this.garlic;
    if (this.garlic) {
      this.toppingsPrice += .50;
    } else {
      this.toppingsPrice -= .50;
    }
    this.totalPrice = this.basePrice + this.toppingsPrice;
  }

  onToggleOlive() {
    this.olive = !this.olive;
    if (this.olive) {
      this.toppingsPrice += .50;
    } else {
      this.toppingsPrice -= .50;
    }
    this.totalPrice = this.basePrice + this.toppingsPrice;
  }
}
