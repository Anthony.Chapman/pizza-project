import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePizzaPageComponent } from './create-pizza-page.component';

describe('CreatePizzaPageComponent', () => {
  let component: CreatePizzaPageComponent;
  let fixture: ComponentFixture<CreatePizzaPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreatePizzaPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePizzaPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
