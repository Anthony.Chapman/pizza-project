import { Component, OnInit, Input } from '@angular/core';
import { Topping } from '../create-pizza-page/Topping';

@Component({
  selector: 'app-pizza-toppings',
  templateUrl: './pizza-toppings.component.html',
  styleUrls: ['./pizza-toppings.component.css']
})
export class PizzaToppingsComponent implements OnInit {
  @Input() topping:Topping;

  constructor() { }

  ngOnInit(): void {
  }

  // Set Dynamic Classes
  setClasses() {
    let classes = {
      topping: true,
      'is-selected': this.topping.selected
    }
    return classes;
  }

  onToggle(topping) {
    console.log(topping);
    topping.selected = !topping.selected;
  }
}
