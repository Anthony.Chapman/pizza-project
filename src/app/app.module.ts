import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { SidebarModule } from 'ng-sidebar';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { InfoBarComponent } from './components/info-bar/info-bar.component';
import { CreatePizzaComponent } from './components/create-pizza/create-pizza.component';
import { PizzasListComponent } from './components/pizzas-list/pizzas-list.component';
import { DeliveryMapComponent } from './components/delivery-map/delivery-map.component';
import { CreatePizzaPageComponent } from './components/create-pizza-page/create-pizza-page.component';
import { HomeComponent } from './components/home/home.component';
import { PizzaToppingsComponent } from './components/pizza-toppings/pizza-toppings.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { PersonaComponent } from './components/persona/persona.component';
import { ContactPageComponent } from './components/contact-page/contact-page.component';
import { CartComponent } from './components/cart/cart.component';

@NgModule({
  declarations: [
    AppComponent,
    InfoBarComponent,
    CreatePizzaComponent,
    PizzasListComponent,
    DeliveryMapComponent,
    CreatePizzaPageComponent,
    HomeComponent,
    PizzaToppingsComponent,
    SidebarComponent,
    AboutUsComponent,
    PersonaComponent,
    ContactPageComponent,
    CartComponent
  ],
  imports: [
    BrowserModule,
    SidebarModule.forRoot(),
    AppRoutingModule,
    CoreModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
