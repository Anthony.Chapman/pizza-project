export interface Cart {
    imgUrl: string;
    name: string;
    ingredients: string;
    quantity: number;
    price: number;
}