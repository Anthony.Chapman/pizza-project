export interface Pizza {
    imgUrl: string;
    name: string;
    badge?: string;
    badgeGreen?: string;
    badgeRed?: string;
    ingredients: string;
    price: number;
}
