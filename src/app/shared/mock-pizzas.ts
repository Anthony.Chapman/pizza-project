import { Pizza } from "./pizza";

export const PIZZAS: Pizza[] = [
    {
        imgUrl: 'assets/img/pizza-mafioso.png',
        name: "Mafioso",
        badge: "Popular",
        ingredients: "Sauce, onion, paprika, cheese, bacon, champignons, olives",
        price: 6.90
    },
    {
        imgUrl: 'assets/img/pizza-cananas.png',
        name: "Cananas",
        ingredients: "Sauce, pineapple, paprika, cheese, smoked chicken",
        price: 6.90
    },
    {
        imgUrl: 'assets/img/pizza-primavera-verde.png',
        name: "Primavera Verde",
        ingredients: "Arugula, sauce, cherry tomatoes, mozzarella, cured ham, sundried tomatoes",
        price: 8.50
    },
    {
        imgUrl: 'assets/img/pizza-francescana.png',
        name: "Francescana",
        ingredients: "Sauce, paprika, cheese, ham, salted mushrooms",
        price: 6.90
    },
    {
        imgUrl: 'assets/img/pizza-macho.png',
        name: "Macho",
        ingredients: "Garlic, sauce, onion, cheese, bacon, salami, minced meat, champignons, olives, jalapeno, kebab",
        price: 8.50
    },
    {
        imgUrl: 'assets/img/pizza-picante.png',
        name: "Picante",
        badgeRed: "Spicy",
        ingredients: "Sauce, tex-mex sauce, nacho, paprika, cheese, jalapeno, roast beef",
        price: 7.30
    },
    {
        imgUrl: 'assets/img/pizza-vegetariana.png',
        name: "Vegetariana",
        badgeGreen: "No meat",
        ingredients: "Sauce, pineapple, paprika, broccoli, cheese, olives",
        price: 6.30
    },
    {
        imgUrl: 'assets/img/pizza-topolino.png',
        name: "Topolino",
        ingredients: "Sauce, pineapple, cheese, ham, champignons",
        price: 6.70
    },
    {
        imgUrl: 'assets/img/pizza-siciliana.png',
        name: "Siciliana",
        ingredients: "Garlic, sauce, onion, cheese, ham",
        price: 6.70
    },
    {
        imgUrl: 'assets/img/pizza-romana.png',
        name: "Romana",
        ingredients: "Sauce, pineapple, cheese, bacon, blue cheese",
        price: 6.90
    },
    {
        imgUrl: 'assets/img/pizza-pepperone.png',
        name: "Pepperone",
        badgeRed: "Spicy",
        ingredients: "Sauce, onion, tomato, cheese, champignons, pepperoni",
        price: 6.90
    },
    {
        imgUrl: 'assets/img/pizza-margherita.png',
        name: "Margherita",
        badgeGreen: "No meat",
        ingredients: "Sauce, cheese",
        price: 6.20
    },
    {
        imgUrl: 'assets/img/pizza-kebab.png',
        name: "Kebab",
        ingredients: "Sauce, onion, cheese, jalapeno, kebab",
        price: 6.90
    },
    {
        imgUrl: 'assets/img/pizza-hawaij.png',
        name: "Hawaij",
        badgeRed: "Spicy",
        ingredients: "Tex-mex sauce, onion, paprika, corn, cheese, bbq chicken",
        price: 6.90
    },
    {
        imgUrl: 'assets/img/pizza-frutti-di-mare.png',
        name: "Frutti Di Mare",
        ingredients: "Sauce, pineapple, paprika, cheese, surimi, shrimp",
        price: 6.90
    },
    {
        imgUrl: 'assets/img/pizza-bolognese.png',
        name: "Bolognese",
        badgeRed: "Spicy",
        ingredients: "Tex-mex sauce, paprika, cheese, minced meat, jalapeno",
        price: 6.90
    },
    {
        imgUrl: 'assets/img/pizza-americana.png',
        name: "Americana",
        ingredients: "Sauce, pineapple, cheese, salami, olives",
        price: 6.70
    }
];