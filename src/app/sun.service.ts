import { Injectable } from '@angular/core';
import { PIZZAS } from './shared/mock-pizzas';
import { Pizza } from './shared/pizza';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SunService {
  getPizzas(): Observable<Pizza[]> {
    const pizzas = of(PIZZAS);
    return pizzas;
  }

  constructor() { }
}
