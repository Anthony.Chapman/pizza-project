import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Cart } from './shared/cart';
import { CART } from './shared/mock-cart';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  items = [];

  getCart(): Observable<Cart[]> {
    const cart = of(CART);
    return cart;
  }

  addToCart(selectedItem) {
    console.log(selectedItem);
    
    return of (CART.push(selectedItem));
  }

  getItems() {
    return this.items;
  }

  clearCart() {
    this.items = [];
    return this.items;
  }

  constructor() { }
}
